# frozen_string_literal: false

require_relative '../src/gl-infra-15178-git-config-scanner/find_dirty_git_repository_configs'

# rubocop: disable Style/RegexpLiteral
describe GitRepositoryInventory do
  subject { described_class.new(args) }

  let(:git_repository_inventory) { subject }
  let(:args) { Script::Default::CONFIG.dup }
  let(:inventory_file_path) { args[:inventory_file_path] }
  let(:mock_logger) { instance_double('FileLogger') }
  let(:mock_progress_logger) { instance_double('FileLogger') }
  let(:mock_process_status) { instance_double('Process::Status') }
  let(:status) { 0 }
  let(:success) { true }
  let(:repository_01) { '/path/to/git/repository_01' }
  let(:repository_02) { '/path/to/git/repository_02' }
  let(:config_01) { args[:allowed_config] }
  let(:config_02) { 'unexpected.but.acceptable' }
  let(:config_03) { nil }
  let(:config_04) { 'fatal: --local can only be used inside a git repository' }
  let(:config_05) { '' }
  let(:config_06) { 'unanticipated.but.fine' }
  let(:invalid_local_use_error_message) { 'fatal: --local can only be used inside a git repository' }
  let(:unique_config_entries_file_path) { '/tmp/unique_config_entries.json' }

  before do
    allow(mock_logger).to receive(:info)
    allow(mock_progress_logger).to receive(:info)
    allow(mock_process_status).to receive(:exitstatus).and_return(status)
    allow(mock_process_status).to receive(:success?).and_return(success)
    allow(git_repository_inventory).to receive(:init_project_migration_logging).and_return(mock_logger)
    allow(git_repository_inventory).to receive(:compile_repository_inventory)
    allow(git_repository_inventory).to receive(:select_inventory_file).and_return(inventory_file_path)
    allow(git_repository_inventory).to receive(:count_lines).and_return(2)
    allow(git_repository_inventory).to receive(:read_inventory).and_yield(repository_01, 1).and_yield(repository_02, 2)
    allow(git_repository_inventory).to receive(:new_line)
    allow(git_repository_inventory).to receive(:log).and_return(mock_logger)
    allow(git_repository_inventory).to receive(:progress_log).and_return(mock_progress_logger)
  end

  describe '#read_config' do
    context 'when config is empty' do
      let(:status) { 1 }
      let(:success) { false }

      it 'returns empty' do
        expect(git_repository_inventory).to receive(:run_command).with(args[:read_config_command]).and_return(
          ['', '', mock_process_status]
        )
        result = git_repository_inventory.read_config(repository_01)
        expect(result.empty?).to be(true)
      end
    end

    context 'when config is not empty' do
      let(:status) { 0 }
      let(:success) { true }
      let(:expected_result) { 'foo' }

      it 'returns the config details verbatim' do
        expect(git_repository_inventory).to receive(:run_command).with(args[:read_config_command]).and_return(
          [expected_result, '', mock_process_status]
        )
        result = git_repository_inventory.read_config(repository_01)
        expect(result).to eq(expected_result)
      end
    end

    context 'when config command encounters an error' do
      let(:status) { 2 }
      let(:success) { false }

      it 'raises an error' do
        expect(git_repository_inventory).to receive(:run_command).with(args[:read_config_command]).and_return(
          ['', invalid_local_use_error_message, mock_process_status]
        )
        expect do
          git_repository_inventory.read_config(repository_01)
        end.to raise_error(RuntimeError, invalid_local_use_error_message)
      end
    end
  end

  describe '#find_empty_configs' do
    context 'when details are acceptable' do
      it 'persists data' do
        expect(git_repository_inventory).to receive(:process_inventory).and_call_original
        expect(git_repository_inventory).to receive(:analyze_repository_config).twice.and_call_original
        expect(git_repository_inventory).to receive(:clear_config_details)
        expect(git_repository_inventory).to receive(:get_repository_config).and_return(config_01)
        expect(git_repository_inventory).to receive(:get_repository_config).and_return(config_02)
        expect(git_repository_inventory).to receive(:save).twice.and_call_original
        expect(git_repository_inventory).to receive(:persist_data).twice
        expect(git_repository_inventory).to receive(:persist_errors)
        expect(mock_logger).to receive(:info).with('Taking repository inventory')
        expect(mock_logger).to receive(:info).with(/Elapsed time: [\d.e\-]+ seconds/).twice
        expect(mock_logger).to receive(:info).with('Compiling config details')
        expect(mock_logger).to receive(:info).with('Empty configs: 0/2')
        expect(mock_logger).to receive(:info).with('Errors: 0/2')
        expect(mock_logger).to receive(:info).with('Unique config entries: /tmp/unique_config_entries.json')
        expect(mock_logger).to receive(:info).with('Untidy configs: /tmp/config_details.json')
        result = git_repository_inventory.find_empty_configs
        expect(result).to be_nil
      end
    end

    context 'when inventory is cached' do
      it 'compiles repository inventory' do
        allow(git_repository_inventory).to receive(:use_latest_cached_inventory).and_return(true)
        expect(git_repository_inventory).not_to receive(:compile_repository_inventory)
        expect(git_repository_inventory).to receive(:with_report).once
        result = git_repository_inventory.find_empty_configs
        expect(result).to be_nil
      end
    end

    context 'when some details are invalid' do
      it 'persists and reports errors' do
        expect(git_repository_inventory).to receive(:process_inventory).and_call_original
        expect(git_repository_inventory).to receive(:analyze_repository_config).twice.and_call_original
        expect(git_repository_inventory).to receive(:clear_config_details)
        expect(git_repository_inventory).to receive(:get_repository_config).and_return(config_03)
        expect(git_repository_inventory).to receive(:get_repository_config).and_raise(
          RuntimeError, invalid_local_use_error_message)
        expect(git_repository_inventory).to receive(:save).twice.and_call_original
        expect(git_repository_inventory).to receive(:persist_data)
        expect(git_repository_inventory).to receive(:persist_errors)
        expect(mock_logger).to receive(:info).with('Taking repository inventory')
        expect(mock_logger).to receive(:info).with(/Elapsed time: [\d.e\-]+ seconds/).twice
        expect(mock_logger).to receive(:info).with('Compiling config details')
        expect(mock_logger).to receive(:info).with('Empty configs: 1/2')
        expect(mock_logger).to receive(:info).with('Errors: 1/2')
        expect(mock_logger).to receive(:info).with(/Error log: \/tmp\/rspec-errors-[\d\-_]+.json/)
        expect(mock_logger).to receive(:info).with('Unique config entries: /tmp/unique_config_entries.json')
        expect(mock_logger).to receive(:info).with('Untidy configs: /tmp/config_details.json')
        result = git_repository_inventory.find_empty_configs
        expect(git_repository_inventory.errors).to eq(
          [
            {
              error: "fatal: --local can only be used inside a git repository",
              repository_path: "/path/to/git/repository_02"
            }
          ]
        )
        expect(result).to be_nil
      end
    end

    context 'when some details are empty' do
      it 'persists only the non-empty details' do
        expect(git_repository_inventory).to receive(:process_inventory).and_call_original
        expect(git_repository_inventory).to receive(:analyze_repository_config).twice.and_call_original
        expect(git_repository_inventory).to receive(:clear_config_details)
        expect(git_repository_inventory).to receive(:get_repository_config).and_return(config_05)
        expect(git_repository_inventory).to receive(:get_repository_config).and_return(config_06)
        expect(git_repository_inventory).to receive(:save).twice.and_call_original
        expect(git_repository_inventory).to receive(:persist_data).twice
        expect(git_repository_inventory).to receive(:persist_errors)
        expect(mock_logger).to receive(:info).with('Taking repository inventory')
        expect(mock_logger).to receive(:info).with(/Elapsed time: [\d.e\-]+ seconds/).twice
        expect(mock_logger).to receive(:info).with('Compiling config details')
        expect(mock_logger).to receive(:info).with('Empty configs: 1/2')
        expect(mock_logger).to receive(:info).with('Errors: 0/2')
        expect(mock_logger).to receive(:info).with('Unique config entries: /tmp/unique_config_entries.json')
        expect(mock_logger).to receive(:info).with('Untidy configs: /tmp/config_details.json')
        result = git_repository_inventory.find_empty_configs
        expect(git_repository_inventory.errors).to eq([])
        expect(result).to be_nil
      end
    end
  end
end
# rubocop: enable Style/RegexpLiteral
