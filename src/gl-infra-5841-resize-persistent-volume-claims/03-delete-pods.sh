#! /usr/bin/env bash

set -euo pipefail

set -x

for i in $(seq 0 14); do
    echo " Deleting pod: pod/thanos-store-$i-0"
    read -p "Continue? (y/N)" -n 1 -r
    echo
    [[ $REPLY =~ ^[Yy]$ ]] || break

    kubectl --namespace=monitoring delete pod/thanos-store-$i-0
    echo "Waiting for pod recreation..."
    kubectl --namespace=monitoring wait --for=condition=Ready pod/thanos-store-$i-0 --timeout=420s
    kubectl --namespace=monitoring get pod | grep thanos

    echo " Deleting pod: pod/thanos-store-$i-1"
    read -p "Continue? (y/N)" -n 1 -r
    echo
    [[ $REPLY =~ ^[Yy]$ ]] || break

    kubectl --namespace=monitoring delete pod/thanos-store-$i-1
    echo "Waiting for pod recreation..."
    kubectl --namespace=monitoring wait --for=condition=Ready pod/thanos-store-$i-1 --timeout=420s
    kubectl --namespace=monitoring get pod | grep thanos
done
