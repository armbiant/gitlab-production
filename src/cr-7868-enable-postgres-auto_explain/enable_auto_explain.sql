-- Enable auto_explain for 1% of queries with a 5-second logging threshold and full details except log_timing off/false.
ALTER SYSTEM SET auto_explain.sample_rate = 0.01 ;
ALTER SYSTEM SET auto_explain.log_min_duration = '5s' ;
ALTER SYSTEM SET auto_explain.log_analyze = true ;
ALTER SYSTEM SET auto_explain.log_timing = false ;
ALTER SYSTEM SET auto_explain.log_buffers = true ;
ALTER SYSTEM SET auto_explain.log_settings = true ;
