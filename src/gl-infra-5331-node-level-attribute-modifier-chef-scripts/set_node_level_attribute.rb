#! /usr/bin/env ruby
# frozen_string_literal: false

# For: https://gitlab.com/gitlab-com/gl-infra/production/-/issues/5377

# Remember! The new_node_attribute_section_attributes are being add to
# the _"normal"_ attributes, and _not_ the "default"
# attributes. The "default" attributes define the cattle node state.
# Wheras the _"normal"_ attributes define the pet node state. This script
# is temporarily turning a cattle node into a pet node.

new_node_attribute_section_name = 'gitlab_fluentd'
new_node_attribute_section_attributes = {
  'postgres_pg_stat_statements_enable': true,
  'postgres_pg_stat_activity_enable': true
}

target_node_name = ENV['TARGET_NODE']
dry_run = (ENV['DRY_RUN'] != 'false' && ENV['DRY_RUN'] != 'no')
unless target_node_name.nil? || target_node_name.empty?
  nodes.find(name: target_node_name) do |node|
    if dry_run
      puts "[Dry-run] Would have added '#{new_node_attribute_section_name}' to normal attributes of node #{node.name}"
    else
      puts "Adding '#{new_node_attribute_section_name}' to normal attributes of node #{node.name}"
      new_node_attribute_section_attributes.each do |key, value|
        # Using the key-value accessor method will implicitly create a
        # new VividHash node-level attribute sub-section.
        # Reference: https://github.com/chef/chef/blob/2eb1c0ad50fa1680536b8ef78237c188e6cd34ac/lib/chef/node/attribute_collections.rb#L134
        node.normal_attrs[new_node_attribute_section_name][key] = value
      end
      node.save
    end
  end
end
