# Production Change - Criticality 2 ~C2

## Change Summary

This production issue is to be used for Gamedays as well as recovery in case of a zonal outage.
It outlines the steps to be followed when adding new patroni replicas in a new zone.

### Gameday execution roles and details

| Role | Assignee |
|---|---|
| Change Technician | <!-- woodhouse: '`@{{ .Username }}`' --> |
| Change Reviewer | <!-- woodhouse: '@{{ .Reviewer }}' --> |

- **Services Impacted**  - ~"Service::PatroniV14" ~"Service::PatroniCiV14" ~"Service::PatroniRegistryV14"
- **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' --> 90 minutes
- **Downtime Component** - <!-- woodhouse: '{{ .Downtime }}'--> 30 minutes

{+ Provide a brief summary indicating the affected zone +}

## [**For Gamedays only**] Preparation Tasks

1. [ ] One week before the gameday make an announcement on slack [production_engineering](https://gitlab.enterprise.slack.com/archives/C03QC5KNW5N) channel. Consider also posting this in the appropriate environment channels, [staging](https://gitlab.enterprise.slack.com/archives/CLM200USV) or [production](https://gitlab.enterprise.slack.com/archives/C101F3796).

    - **Example message:**

    ```code
      Next week on [DATE & TIME] we will be executing a Traffic Routing game day. The process will involve increasing the capacity for Patroni. This should take
      approximately 90 minutes, the aim for this exercise is to test our disaster recovery capabilities and measure if we are still within
      our RTO & RPO targets set by the [DR working group](https://handbook.gitlab.com/handbook/company/working-groups/disaster-recovery/)
      for GitLab.com.
      See https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17274
    ```

1. [ ] Add an event to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
1. [ ] Notify the release managers on _Slack_ by mentioning `@release-managers` and referencing this issue and await their acknowledgment.
1. [ ] Notify the eoc on _Slack_ by mentioning `@sre-oncall` and referencing this issue and wait for approval by adding the ~eoc_approved label.
    - **Example message:**

    ```code
    @release-managers or @sre-oncall LINK_TO_THIS_CR is scheduled for execution. 
    We will be adding a patroni replica for approximately 30 minutes. Kindly review and approve the CR
    ```

1. [ ] Post an FYI link of the slack message to the [#test-platform](https://gitlab.enterprise.slack.com/archives/C3JJET4Q6) channel on slack.

## Detailed steps for the change

### Change Steps - steps to take to execute the change

#### Execution

1. [ ] If you are conducting a practice (Gameday) run of this, consider starting a recording of the process now.

1. [ ] Note the start time in UTC in a comment to record this process duration.

1. [ ] Set label ~"change::in-progress" `/label ~change::in-progress`

1. Create the MR to provision Patroni replicas. ❗❗NOTE❗❗ **GPRD Merge requests should target a single zonal outage recovery branch, to be determined and created beforehand. NOT master**
    - [ ] Create the MR
    - **Example MRs:**
      - `gstg` https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/7325
      - `gprd` https://ops.gitlab.net/gitlab-com/gl-infra/config-mgmt/-/merge_requests/7656
    - [ ] Get the MR approved
    - [ ] Apply the MR

1. [ ] **GPRD only** Wait for the single zonal outage recovery branch to be merged into master.

❗❗ **NOTE:** ❗❗ _Merging the changes into master can take a while to complete, it can take up to 30 minutes before you are able to log in to the new replica._

#### Validation of the Patroni replicas

1. [ ] Wait for the instances to be built and Chef to converge.
    - Staging: [Confirm that chef runs have completed](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D\~%22gitaly%7Cpatroni%7Cpatroni-ci%7Cpatroni-registry%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D) for new patroni replicas it can take upto 30 minutes before they show up.
      <details><summary>Trouble shooting tips</summary>

      - Tail the serial output to confirm that the start up script executed successfully.
      `gcloud compute --project=$project instances tail-serial-port-output $instance_name --zone=$zone --port=1`
      the variables `$project` represents the project `gitlab-staging-1` for the patroni replicas, `$instance_name` represent the instance e.g `patroni-main-v14-106-db-gstg` and `$zone` represents the recovery zone e.g `us-east1-c`.
      - We could also tail bootstrap logs example: `tail -f /var/tmp/bootstrap-20231108-133642.log`

    </details>

1. [ ] Once the patroni replicas are ready ssh into each replica and start patroni:
    - `sudo systemctl enable patroni && sudo systemctl start patroni`

1. [ ] Note the conclusion time in UTC in a comment to record this process duration.

#### [**For Gamedays only**] Clean up

- [ ] Revert the MR that adds Patroni replicas with zone overrides, this is the MR that was merged at execution step 2.
    ❗❗**NOTE**❗❗ _when reverting the above :arrow_up: MR as of this [gameday](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/17507) you need to comment `atlantis approve_policies` in the MR to bypass the policies before applying with `atlantis apply`. You also need to disable the all pipelines must pass condition in the MR settings._

#### Wrapping up

- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is complete.
- [ ] Set label ~"change::complete" `/label ~change::complete`
- [ ] Compile the real time measurement of this process and update the [Recovery Measrements Runbook](https://gitlab.com/gitlab-com/runbooks/-/blob/master/docs/disaster-recovery/recovery-measurements.md?ref_type=heads).

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

#### *It is estimated that this will take 5m to complete*

- [ ] Notify the `@release-managers` and `@sre-oncall` that the exercise is aborted.
- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

- Completed Chef runs: [Staging](https://thanos-query.ops.gitlab.net/graph?g0.expr=max(chef_client_last_run_timestamp_seconds%7Benv%3D%22gstg%22%2C%20type%3D%22gitaly%22%7D)%20by%20(fqdn)&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)
- Patroni service: [Staging](https://dashboards.gitlab.net/goto/S8D8Kz7SR?orgId=1)
- PostgreSQL Overview: [Staging](https://dashboards.gitlab.net/goto/pNYqFk7SR?orgId=1)
- Replication Lag: [Staging](https://thanos.gitlab.net/graph?g0.expr=pg_replication_lag%7Benv%3D%22gstg%22%2Cfqdn%3D~%22patroni-main-v14-.*%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=2h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D)

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:

- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:

- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
    - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed upon with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, use the `@sre-oncall` handle in slack
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results are noted in a comment on this issue.
  - A dry-run has been conducted and results are noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed before the change is rolled out. (In the #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In the #production channel, mention `@release-managers` and this issue and await their acknowledgement.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.

/label ~"C2"
/label ~"change::unscheduled"
/label ~"change"
/label ~"release-blocker"
/label ~"blocks deployments"
/label ~"Deploys-blocked-gstg"
