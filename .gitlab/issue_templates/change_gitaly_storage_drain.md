<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.

The following placeholders are in this template and need to be updated:

- CHANGE_ISSUE: Link to this change issue after it is created
- STORAGE_NAME: Name of storages being drained. e.g., file-100
- HELMFILES_MR: MR to update gitalyctl configuration. e.g., https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/merge_requests/3294
- THANOS_REPO_COUNT: Link to Thanos query for repository counts. e.g., https://thanos.gitlab.net/graph?g0.expr=gitaly_total_repositories_count%7Bfqdn%3D~%22file-10%5B89%5D-stor-gprd.c.gitlab-production.internal%22%7D&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D

NOTE: This is a temporary change template and be removed once https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/935 is closed.
-->

/label ~"WG::DisasterRecovery" ~"C3" ~"change::unscheduled" ~"change"
/confidential
/epic https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/1054

# Production Change

### Change Summary

* Epic: https://gitlab.com/groups/gitlab-com/gl-infra/-/epics/935
* Design Documentation: https://gitlab.com/gitlab-com/gl-infra/readiness/-/blob/master/library/gitaly-multi-project/README.md

### Change Details

1. **Services Impacted**  - ~"Service::Gitaly"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - 7 days
1. **Downtime Component** - N/A

### Change Steps - steps to take to execute the change

*Estimated Time to Complete* - 7 days

- [ ] Calculate if you need to provision new Gitaly nodes
    - [AVAILABLE_DISK_SPACE](https://thanos.gitlab.net/graph?g0.expr=sum%20by%20(environment%2C%20tier%2C%20type%2C%20stage%2C%20shard%2C%20shard)%20(%0A%20%20(%0A%20%20%20%20node_filesystem_avail_bytes%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20fqdn%3D~%22gitaly-.*-stor-.*%22%2Cmountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2Cshard%3D~%22default%7Cpraefect%22%7D%20-%0A%20%20%20%20(node_filesystem_size_bytes%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20fqdn%3D~%22gitaly-.*-stor-.*%22%2Cmountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2Cshard%3D~%22default%7Cpraefect%22%7D%20*%200.25)%0A%20%20)%0A%20%20and%0A%20%20(instance%3Anode_filesystem_avail%3Aratio%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20fqdn%3D~%22gitaly-.*-stor-.*%22%2Cmountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2Cshard%3D~%22default%7Cpraefect%22%7D%20%3E%200.25)%0A)%20%2F%201024%20%2F%201024%20%2F1024%20%2F1024%20%0A%0A-%20%0A%0A60&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D): This checks the availabile Gitaly nodes that are less then [75% full](https://gitlab.com/gitlab-com/gl-infra/gitaly-shard-weights-assigner/-/blob/18f9e0a81775bdf84cc3b82942c9e8d2d57c8b71/assigner.rb#L9). It also subtracts 60TB from the total to reserve for new projects.
    - [USED_DISK_SPACE](https://thanos.gitlab.net/graph?g0.expr=(node_filesystem_size_bytes%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20fqdn%3D~%22file-90.*%22%2Cmountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2Cshard%3D~%22default%7Cpraefect%22%7D%0A-%20%0Anode_filesystem_free_bytes%7Benv%3D%22gprd%22%2Cstage%3D%22main%22%2Ctype%3D%22gitaly%22%2C%20fqdn%3D~%22file-90.*%22%2Cmountpoint%3D%22%2Fvar%2Fopt%2Fgitlab%22%2Cshard%3D~%22default%7Cpraefect%22%7D)%20%2F%201024%20%2F%201024%20%2F%201024%20%2F%201024&g0.tab=1&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D): Update the `fqdn` with the storage(s) you want to drain.
    - AVAILABLE_DISK_SPACE - USED_DISK_SPACE: If it's higher then `0` then you have enough diskspace to drain the storage.
- [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
- [ ] Create silence for [`GitalyServiceGoserverTrafficCessationSingleNode|GitalyServiceGoserverTrafficAbsentSingleNode`](https://alerts.gitlab.net/#/silences/new?filter={alertname%3D~%22GitalyServiceGoserverTrafficCessationSingleNode%7CGitalyServiceGoserverTrafficAbsentSingleNode%22%2Ctype%3D%22gitaly%22%2Cenv%3D%22gprd%22%2Cfqdn%3D%22%24STORAGE_NAME%22}) on the affected storages. Update `fqdn` and set the duration for 4w
- [ ] Inform the on-call engineer

```text
@sre-oncall FYI, we are about to start migrating storages to the new Gitaly servers, for details see CHANGE_ISSUE. We will be draining two storages, `STORAGE_NAME`
```

- [ ] Merge configuration update to start draining :point_right: HELMFILES_MR
- [ ] Monitor for [repositories on STORAGE_NAME](THANOS_REPO_COUNT) are drained. This number will likely never reach zero, to confirm that there are no more projects in the database on the corresponding storage run the following:
```
ssh postgres-dr-main-v14-archive-01-db-gprd.c.gitlab-production.internal "sudo gitlab-psql -c \"SELECT repository_storage, COUNT(*) FROM projects  WHERE repository_storage LIKE 'nfs-file%' GROUP BY repository_storage;\"" |  sed -e 's/nfs-file\(.\)\(.\) /nfs-file0\1\2/' | sort
```
- [ ] Once there are zero projects, open a new change request `/change declare` and select the `change_gitaly_storage_delete.md` template. Relate it to this change issue.
- [ ] Set label ~"change::complete" `/label ~change::complete`

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete* - 5 minutes

- [ ] Set [`enabled` to `false`](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-helmfiles/-/blob/29046286e74b19015757c806f80b35c918ce60cf/releases/gitalyctl/gprd.yaml.gotmpl#L2)
- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe


- Metric: Multi Project Overview
  - Location: https://dashboards.gitlab.net/d/gitaly-multi-project-move/gitaly-gitaly-multi-project-move?orgId=1
- Metric: Move Errors
  - Location: https://dashboards.gitlab.net/explore?orgId=1&left=%7B%22datasource%22:%22R8ugoM-Vk%22,%22queries%22:%5B%7B%22refId%22:%22A%22,%22expr%22:%22%7Bnamespace%3D%5C%22gitalyctl%5C%22%7D%20%7C%20json%20level%3D%5C%22level%5C%22%20%7C%20level%20%3D%20%60error%60%22,%22queryType%22:%22range%22,%22datasource%22:%7B%22type%22:%22loki%22,%22uid%22:%22R8ugoM-Vk%22%7D,%22editorMode%22:%22builder%22%7D%5D,%22range%22:%7B%22from%22:%22now-3h%22,%22to%22:%22now%22%7D%7D
- Metric: Gitaly node CPU usage
  - Location: https://thanos.gitlab.net/graph?g0.expr=avg(instance%3Anode_cpu_utilization%3Aratio%7Benv%3D%22gprd%22%2Cenvironment%3D%22gprd%22%2Cfqdn%3D~%22gitaly-.*%22%2Ctype%3D%22gitaly%22%7D)%20by%20(fqdn)%0A&g0.tab=0&g0.stacked=0&g0.range_input=1h&g0.max_source_resolution=0s&g0.deduplicate=1&g0.partial_response=0&g0.store_matches=%5B%5D
- Metric: Queue Detail
  - Location: https://dashboards.gitlab.net/d/sidekiq-queue-detail/sidekiq-queue-detail?orgId=1&var-PROMETHEUS_DS=Global&var-environment=gprd&var-stage=main&var-queue=gitaly_throttled

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C4 ~C3 ~C2 ~C1:
- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.

~C2 ~C1:
- [ ] Check if the following applies:
  - The complexity of the plan is appropriate for the corresponding risk of the change. (i.e. the plan contains clear details).
  - The change plan includes success measures for all steps/milestones during the execution.
  - The change adequately minimizes risk within the environment/service.
  - The performance implications of executing the change are well-understood and documented.
  - The specified metrics/monitoring dashboards provide sufficient visibility for the change.
      - If not, is it possible (or necessary) to make changes to observability platforms for added visibility?
  - The change has a primary and secondary SRE with knowledge of the details available during the change window.
  - The change window has been agreed with Release Managers in advance of the change. If the change is planned for APAC hours, this issue has an agreed pre-change approval.
  - The labels ~"blocks deployments" and/or ~"blocks feature-flags" are applied as necessary.

## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results noted in a comment on this issue.
  - A dry-run has been conducted and results noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - For ~C1 and ~C2 change issues, the change event is added to the [GitLab Production](https://calendar.google.com/calendar/embed?src=gitlab.com_si2ach70eb1j65cnu040m3alq0%40group.calendar.google.com) calendar.
  - For ~C1 and ~C2 change issues, the SRE on-call has been informed prior to change being rolled out. (In #production channel, mention `@sre-oncall` and this issue and await their acknowledgement.)
  - For ~C1 and ~C2 change issues, the SRE on-call provided approval with the ~eoc_approved label on the issue.
  - For ~C1 and ~C2 change issues, the Infrastructure Manager provided approval with the ~manager_approved label on the issue.
  - Release managers have been informed prior to any C1, C2, or ~"blocks deployments" change being rolled out. (In #production channel, mention `@release-managers` and this issue and await their acknowledgment.)
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
  - If the change involves doing maintenance on a database host, an appropriate silence targeting the host(s) should be added for the duration of the change.
