<!--
Please review https://about.gitlab.com/handbook/engineering/infrastructure/change-management/ for the most recent information on our change plans and execution policies.
-->

# Production Change

/label ~"group::global search" ~"Service::Elasticsearch" ~"C3" ~"change::unscheduled" ~"change"

### Change Summary

{+ Provide a high-level summary of the change and its purpose. +}

Related to {+ issue URL +}

We want to resize the {+ Index name(s) +} index shard size setting and reindex using the Zero downtime reindexing feature.

### Change Details

1. **Services Impacted**  - ~"Service::Elasticsearch"
1. **Change Technician**  - <!-- woodhouse: '`@{{ .Username }}`' -->{+ DRI for the execution of this change +}
1. **Change Reviewer**    - <!-- woodhouse: '@{{ .Reviewer }}' -->{+ DRI for the review of this change +}
1. **Time tracking**      - <!-- woodhouse: '{{ .Duration}}' -->{+ Time, in minutes, needed to execute all change steps, including rollback +}

## Detailed steps for the change

### Pre-Change Steps

- [ ] Determine if cluster needs to be scaled
   ```ruby
     elastic_helper = ::Gitlab::Elastic::Helper.default
     target_classes = [{+ List of classes+ }] # example: [Repository, Issue]
     
     current_size = target_classes.sum do |klass|
       name = elastic_helper.klass_to_alias_name(klass: klass)
       elastic_helper.index_size_bytes(index_name: name)
     end
     
     expected_free_size = current_size * 2
     elastic_helper.cluster_free_size_bytes
     elastic_helper.cluster_free_size_bytes > expected_free_size # if true, cluster does not need to scale
   ```
If scaling needs to occur, perform the following steps:

- [ ] In the [Elastic Cloud UI](https://cloud.elastic.co/deployments/f2f6ef644c28466cb6653383369d7160), click Edit for the production deployment
- [ ] Take a screenshot of the existing and proposed settings and add in an **internal comment** 

### Change Steps - steps to take to execute the change

**Note: this change is being applied in production only**

- [ ] Set label ~"change::in-progress" `/label ~change::in-progress`
---
- [ ] Add silences via https://alerts.gitlab.net/#/silences/new with a matcher on env and alert name for each pair:
  - `env="gprd"`, `alertname="SearchServiceElasticsearchIndexingTrafficAbsent"`
  - `env="gprd"`, `alertname="gitlab_search_indexing_queue_backing_up"` 
  - `env="gprd"`, `alertname="SidekiqServiceGlobalSearchIndexingApdexSLOViolation"` 
  - `env="gprd"`, `alertname="SearchServiceGlobalSearchIndexingTrafficCessation"`
- [ ] (optional) Scale the cluster using the settings determined above
  - [ ] Pause indexing
    ```ruby
      ::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: true)
    ```
  - [ ] In the [Elastic Cloud UI](https://cloud.elastic.co/deployments/f2f6ef644c28466cb6653383369d7160), click Edit for the production deployment
  - [ ] Select the required specs and click Apply. Wait until the changes have applied successfully
  - [ ] Verify search operations:
    - [ ] Ensure Enable exact code search is disabled in your user preference setting
    - [ ] [Search for code](https://gitlab.com/search?group_id=1112072&project_id=7444821&scope=blobs&search=elasticsearch+cluster)
    - [ ] [Search for comments](https://gitlab.com/search?group_id=1112072&project_id=7444821&scope=notes&search=elasticsearch+cluster)
  - [ ] Unpause indexing
    ```ruby
      ::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: false)
    ```
  - [ ] Verify indexing:
    - [ ] Add code to a test project, verify it is searchable (may take time depending on how backedup indexing is)   
    - [ ] Add a comment to a test issue, verify it is searchable (may take time depending on how backedup indexing is)
- [ ] For each index: Take a screenshot of [ES monitoring cluster index advanced metrics for last 7 days](https://00a4ef3362214c44a044feaa539b4686.us-central1.gcp.cloud.es.io:9243/app/monitoring#/elasticsearch/indices?_g=(cluster_uuid:nkvJVBhsSwWfoqyHIA_raQ,refreshInterval:(pause:!f,value:10000),time:(from:now-7d%2Fd,to:now))) and attach to an **Internal** comment on this issue
- [ ] For each index, find the current number of shards and attach to a comment on this issue
   ```ruby
    Elastic::IndexSetting.find_by(alias_name: '{+ index alias +}').number_of_shards
  ```
- [ ] For each index, update the number of shards for the affected indices to `{+ New number of shards +}`
   ```ruby
     Elastic::IndexSetting.find_by(alias_name: '{+ index alias +}').update!(number_of_shards: {+ New number of shards +})
   ```
- [ ] Determine the [slice multiplier](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#slice-multiplier) and [maximum running slices](https://docs.gitlab.com/ee/integration/advanced_search/elasticsearch.html#maximum-running-slices) for each index. The defaults may cause additional load on the cluster and impact search performance, so it's recommended to adjust the values. Set `max_slices_running` to `30` and `slice_multiplier` to `1` if you are unsure of what values to use.
- [ ] Trigger re-index with the specified options and note the timestamp when it was triggered -> `{+ Timestamp +}`
  ```ruby
    Elastic::ReindexingTask.create!(targets: %w[{+ Target index class(s) +}], max_slices_running: {+ Max slices +}, slice_multiplier: {+ Slice multipliers +})
  ```
- [ ] Monitor the status of the reindexing through rails console
  ```ruby
    Elastic::ReindexingTask.current # get current reindexing task
    Elastic::ReindexingTask.current.subtasks # one subtask for each target index
    Elastic::ReindexingTask.current.subtasks.first.slices # slices for each subtask (will be 3 * number of shards created)
  ```
- [ ] Ensure that it has finished successfully, `Elastic::ReindexingTask.current` should return `nil`
- [ ] Note the time when the task finishes -> `{+ Timestamp +}`
- [ ] Wait until the backlog of incremental updates gets below 10,000
   - Chart [`Global search incremental indexing queue depth`](https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1)
- [ ] Test that indexing is functional by creating or updating data. The data type will be dependent on which indices are being reindexed. Then search for it to ensure indexing still works (can take up to 2 minutes before it shows up in the search results). Update the list below to include each index for validation
   * [ ] `{+ index name or class +}`
- [ ] (optional) Manually delete the old indices. The newer indices will have a newer date for the suffix. Confirm that the index being removed is not receiving any search or indexing traffic.
   ```shell
   curl -XDELETE "$CLUSTER_URL/$INDEX_NAME"` # Example: gitlab-production-projects-20221020-2340
   ```
- [ ] (optional) Scale down the cluster back down to the original settings (this requires pausing indexing again)
  - [ ] Pause indexing
    ```ruby
      ::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: true)
    ```
  - [ ] In the [Elastic Cloud UI](https://cloud.elastic.co/deployments/f2f6ef644c28466cb6653383369d7160), click Edit for the production deployment
  - [ ] Select the original specs and click Apply. Wait until the changes have applied successfully
  - [ ] Verify search operations:
    - [ ] Ensure Enable exact code search is disabled in your user preference setting
    - [ ] [Search for code](https://gitlab.com/search?group_id=1112072&project_id=7444821&scope=blobs&search=elasticsearch+cluster)
    - [ ] [Search for comments](https://gitlab.com/search?group_id=1112072&project_id=7444821&scope=notes&search=elasticsearch+cluster)
  - [ ] Unpause indexing
    ```ruby
      ::Gitlab::CurrentSettings.update!(elasticsearch_pause_indexing: false)
    ```
  - [ ] Verify indexing:
    - [ ] Add code to a test project, verify it is searchable (may take time depending on how backedup indexing is)   
    - [ ] Add a comment to a test issue, verify it is searchable (may take time depending on how backedup indexing is)
- [ ] Re-enable the slowlog for each index by using the [Dev Console](https://gprd-indexing-20220523.kb.us-central1.gcp.cloud.es.io:9243/app/dev_tools#/console) to issue the command below:
   ```json
   PUT /<index_name>/_settings
   {
	   "index.search.slowlog.threshold.query.warn": "30s",
     "index.search.slowlog.threshold.query.info": "10s",
     "index.search.slowlog.threshold.query.debug": "-1",
     "index.search.slowlog.threshold.query.trace": "-1",
     "index.search.slowlog.threshold.fetch.warn": "30s",
     "index.search.slowlog.threshold.fetch.info": "10s",
     "index.search.slowlog.threshold.fetch.debug": "-1",
     "index.search.slowlog.threshold.fetch.trace": "-1"
   }
   ```
---
- [ ] Remove the alert silences
- [ ] Set label ~"change::complete" `/label ~change::complete`

## Rollback

### Rollback steps - steps to be taken in the event of a need to rollback this change

*Estimated Time to Complete (mins)* - 60

- If the ongoing reindex is consuming too many resources it is possible to [throttle the running reindex](https://www.elastic.co/guide/en/elasticsearch/reference/current/docs-reindex.html#docs-reindex-rethrottle) :
- You can check the index write throughput in [ES monitoring cluster](https://00a4ef3362214c44a044feaa539b4686.us-central1.gcp.cloud.es.io:9243/app/monitoring#/elasticsearch/indices?_g=(cluster_uuid:nkvJVBhsSwWfoqyHIA_raQ,refreshInterval:(pause:!f,value:10000),time:(from:now-7d%2Fd,to:now))) to determine a sensible throttle. Since it defaults to no throttling, it's safe to just set some throttle and observe the impact
    ```shell
      curl -XPOST "$CLUSTER_URL/_reindex/$TASK_ID/_rethrottle?requests_per_second=500
    ```
- If reindexing task fails, it will automatically revert to the original index
- If reindexing task completes, but you need to rollback.
    - [ ] Pause indexing (if it’s not paused already)
    - [ ] Switch the alias back to the original index
    - [ ] Ensure any updates that only went to Destination index are replayed against Source Cluster by searching the logs for the updates https://gitlab.com/gitlab-org/gitlab/-/blob/e8e2c02a6dbd486fa4214cb8183d428102dc1156/ee/app/services/elastic/process_bookkeeping_service.rb#L23 and triggering those updates again using ProcessBookkeepingService#track as well as any updates that went through SideKiq workers `ElasticDeleteProjectWorker`
- [ ] Delete incomplete indices by running, the newer indices will have a newer date for the suffix. Confirm that the index being removed is not receiving any search or indexing traffic.
   ```shell
   curl -XDELETE "$CLUSTER_URL/$INDEX_NAME"` # Example: gitlab-production-projects-20221020-2340
   ```
- [ ] Set label ~"change::aborted" `/label ~change::aborted`

## Monitoring

### Key metrics to observe

<!--
  * Describe which dashboards and which specific metrics we should be monitoring related to this change using the format below.
-->

- Metric: Elasticsearch cluster health
  - Location: https://00a4ef3362214c44a044feaa539b4686.us-central1.gcp.cloud.es.io:9243/app/monitoring#/overview?_g=(cluster_uuid:nkvJVBhsSwWfoqyHIA_raQ,refreshInterval:(pause:!f,value:10000),time:(from:now-15m,to:now)))
  - What changes to this metric should prompt a rollback: Unhealthy nodes/indices that do not recover
- Metric: Elasticsearch monitoring in Grafana
  - Location: https://dashboards.gitlab.net/d/search-main/search-overview?orgId=1
- Metric: `Sidekiq Queues (Global Search)` Indexing queues
  - Location: https://dashboards.gitlab.net/d/sidekiq-main/sidekiq-overview?orgId=1
  - What changes to this metric should prompt a rollback: After unpausing the indexing is failing and the queues are constantly growing

## Change Reviewer checklist

<!--
To be filled out by the reviewer.
-->
~C3:
- [ ] Check if the following applies:
  - The **scheduled day and time** of execution of the change is appropriate.
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - The change plan includes **estimated timing values** based on previous testing.
  - The change plan includes a viable [rollback plan](#rollback).
  - The specified [metrics/monitoring dashboards](#key-metrics-to-observe) provide sufficient visibility for the change.


## Change Technician checklist

<!--
To find out who is on-call, in #production channel run: /chatops run oncall production.
-->

- [ ] Check if all items below are complete:
  - The [change plan](#detailed-steps-for-the-change) is technically accurate.
  - This Change Issue is linked to the appropriate Issue and/or Epic
  - Change has been tested in staging and results noted in a comment on this issue.
  - A dry-run has been conducted and results noted in a comment on this issue.
  - The change execution window respects the [Production Change Lock periods](https://about.gitlab.com/handbook/engineering/infrastructure/change-management/#production-change-lock-pcl).
  - There are currently no [active incidents](https://gitlab.com/gitlab-com/gl-infra/production/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name[]=Incident%3A%3AActive) that are ~severity::1 or ~severity::2
