# vim:set ft=dockerfile:
FROM ruby:3.2.2-alpine

COPY Gemfile .

RUN \
  apk add --update build-base \
  && gem install bundler --no-document \
  && bundle config --local path vendor/bundle \
  && bundle install --path vendor/bundle --retry 3 \
  && gem install --no-document json \
  && gem install --no-document yaml-lint
